import webApp
from urllib.parse import parse_qs

# --- HTML PARA LAS DIFERENTES SITUACIONES

# -- Cuando el cliente no ponga ningun recurso
PAGE_GET1 = '<div style="display:flex; justify-content:center; align-items:center; height:60%; text-align:center;">' \
            '<div style="background-color: #E8F8F5; padding: 50px; display: inline-block; border: 1px solid #F8C471;">'\
            '<meta charset="UTF-8">' \
            '<h1 style="color:#F8C471;">Bienvenido a Content POST App!</h1>' \
            '<p>Si quieres añadir algún recurso a la página, prueba el formulario!:</p>' \
            '<form action="/" method="POST" style="text-align:center; ' \
            'display:flex; flex-direction:column; align-items:center;">' \
            '<label for="recurso">Recurso:</label>' \
            '<input name="recurso" id="recurso" type="text" required style="margin-bottom:10px;">' \
            '<label for="contenido">Contenido:</label>' \
            '<textarea name="contenido" id="contenido" type="text" required></textarea>' \
            '<input type="submit" name="boton" value="Enviar">' \
            '<p style="text-align:center; font-family:\'Bubblegum Sans\', ' \
            'sans-serif;">Si no, puedes elegir entre los siguientes recursos:</p>' \
            '<p style="text-align:center;">{recursos}</p>'\
            '</form></div>'


# -- Cuando el cliente ponga algun recurso.
PAGE_GET2 = '<div style="display:flex; justify-content:center; align-items:center; height:50%; text-align:center;">' \
            '<div style="background-color: #E8F8F5; padding: 50px; display: inline-block; border: 1px solid #F8C471;">'\
            '<meta charset="UTF-8">'\
           '<h1 style="color:#F8C471; font-family: \'Bubblegum Sans\', sans-serif;">Estamos en: {recurso}</h1>' \
           '<p style="text-align:center;">{contenido}</p>' \
           '<p style="text-align:center;">¿Quieres añadir alguna petición?</p>' \
           '<form action="/" method="POST" style="text-align:center; ' \
            'display:flex; flex-direction:column; align-items:center;">' \
            '<label for="recurso">Recurso:</label>' \
            '<input name="recurso" id="recurso" type="text" required style="margin-bottom:10px;">' \
            '<label for="contenido">Contenido:</label>' \
            '<textarea name="contenido" id="contenido" type="text" required></textarea>' \
            '<input type="submit" name="boton" value="Enviar"></form>' \
           '</div>'


# -- Cuando no se de ninguna opcion
PAGE_NOT_FOUND = '<meta charset="UTF-8">' \
                 '<h1 style="color:#FF6347; text-align:center; font-family:\'Bubblegum Sans\',' \
                 'sans-serif;">404 Recurso no existe!</h1>' \
                '<h2 style="text-align:center; font-family:\'Bubblegum Sans\', ' \
                'sans-serif;">Solo puedes elegir entre los siguientes recursos:</h2>' \
                '<h3 style="text-align:center;">{recursos}</h3>'

# -- Cuando el cliente ponga un POST
PAGE_POST = '<div style="display:flex; justify-content:center; align-items:center; height:50%;">' \
            '<div style="background-color: #E8F8F5; padding: 50px; ' \
            'display: inline-block; border: 1px solid #F8C471;">' \
            '<h1 style="color:#F8C471;">El nuevo recurso es: {recurso}</h1>' \
            '<p style="font-family:\'Bubblegum Sans\', sans-serif;">El nuevo contenido es: {contenido}</p>' \
            '</div></div>'

PAGE_UNPROCESABLE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Unprocesable POST: {body}.</p>
  </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Method not allowed: {method}.</p>
  </body>
</html>
"""


# -- CODIGOS HTTP

C200 = '200 OK'
C404 = '404 Not Found'
C422 = '422 Unprocessable Entity'
C405 = '405 Method not allowed'


class ContentPostApp(webApp.webApp):

    recursos_contenido = {
        'inicio': 'Bienvenido a nuestra página de inicio',
        'mascotas': 'En nuestra página de mascotas podrás encontrar información sobre animales de compañía',
        'contacto': '¿Quieres contactar con nosotros? Aquí puedes encontrar nuestra información de contacto',
        'blog': 'En nuestro blog compartimos noticias y artículos sobre diversos temas de interés',
        'tienda': 'Visita nuestra tienda en línea para encontrar productos exclusivos'
    }

    def parse(self, request):
        metodo = request.split(' ')[0]
        recurso = request.split(' ')[1][1:].lower()
        if metodo == "POST":
            cuerpo = request.split("\r\n\r\n")[-1]
        else:
            cuerpo = None
        return metodo, recurso, cuerpo

    def process(self, parsedRequest):

        metodo, recurso, cuerpo = parsedRequest

        if metodo == "GET":
            cuerpo_html, codigo_http = self.do_GET(recurso)
        elif metodo == "POST":
            cuerpo_html, codigo_http = self.do_POST(recurso, cuerpo)
        else:
            cuerpo_html = PAGE_NOT_ALLOWED.format(metodo=metodo)
            codigo_http = C405

        return (codigo_http, '<html><body>' + cuerpo_html + '</body></html>')

    def do_GET(self, recurso):

        if recurso in self.recursos_contenido:
            cuerpo_html = PAGE_GET2.format(recurso=recurso, contenido=self.recursos_contenido[recurso])
            codigo_http = C200
        elif recurso == '':
            cuerpo_html = PAGE_GET1.format(recursos=', '.join(self.recursos_contenido.keys()))
            codigo_http = C200
        else:
            cuerpo_html = PAGE_NOT_FOUND.format(recursos=', '.join(self.recursos_contenido.keys()))
            codigo_http = C404

        return cuerpo_html, codigo_http

    def do_POST(self, recurso, cuerpo):
        # -- Diccionario
        diccionario_cuerpo = parse_qs(cuerpo)
        if recurso == '':
            if ("recurso" in diccionario_cuerpo) and ("contenido" in diccionario_cuerpo):
                recurso = diccionario_cuerpo["recurso"][0].lower()
                contenido = diccionario_cuerpo["contenido"][0]
                self.recursos_contenido[recurso] = contenido
                cuerpo_html = PAGE_POST.format(recurso=recurso, contenido=self.recursos_contenido[recurso])
                codigo_http = C200
            else:
                cuerpo_html = PAGE_UNPROCESABLE.format(cuerpo=cuerpo)
                codigo_http = C422
        else:
            cuerpo_html = PAGE_NOT_ALLOWED.format(method='POST')
            codigo_http = C405

        return cuerpo_html, codigo_http


if __name__ == '__main__':
    testWebApp = ContentPostApp("localhost", 1234)
